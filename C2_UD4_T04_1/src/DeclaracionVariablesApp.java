
public class DeclaracionVariablesApp {

	public static void main(String[] args) {
		
		// Declaración de dos variables numéricas
		int A = 1;
		int B = 2;
		
		// Muestra por consola de operaciones
		System.out.println(A + B);
		System.out.println(A - B);
		System.out.println(A * B);
		System.out.println(A / B);
		System.out.println(A % B);
		
	}

}
